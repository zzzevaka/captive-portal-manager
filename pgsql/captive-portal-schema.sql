CREATE TABLE user_role (
                id SERIAL NOT NULL,
                name VARCHAR(50),
                describe VARCHAR(300),
                show_html_path BOOLEAN NOT NULL,
                create_html_path BOOLEAN NOT NULL,
                delete_html_path BOOLEAN NOT NULL,
                show_template BOOLEAN NOT NULL,
                create_template BOOLEAN NOT NULL,
                delete_template BOOLEAN NOT NULL,
                edit_template BOOLEAN NOT NULL,
                show_hotspot BOOLEAN NOT NULL,
                create_hotspot BOOLEAN NOT NULL,
                delete_hotspot BOOLEAN NOT NULL,
                edit_hotspot BOOLEAN NOT NULL,
                edit_hotspot_ip BOOLEAN NOT NULL,
                PRIMARY KEY (id)
        );

CREATE INDEX ON user_role (id);

CREATE TABLE account (
                id SERIAL NOT NULL,
                name VARCHAR(150),
                start_date TIMESTAMP WITHOUT TIME ZONE NOT NULL, 
                stop_date TIMESTAMP WITHOUT TIME ZONE NOT NULL,
                PRIMARY KEY (id)
        );

CREATE INDEX ON account (id);

CREATE TABLE acc_user (
                id SERIAL NOT NULL,
                username VARCHAR (50) NOT NULL,
                password VARCHAR(150) NOT NULL,
                acc_id INTEGER NOT NULL,
                role_id INTEGER NOT NULL,
                start_date TIMESTAMP WITHOUT TIME ZONE NOT NULL, 
                stop_date TIMESTAMP WITHOUT TIME ZONE NOT NULL, 
                UNIQUE(username),
                PRIMARY KEY (id)
        );

CREATE INDEX ON acc_user (id);
CREATE INDEX ON acc_user (username);
CREATE INDEX ON acc_user (acc_id);


CREATE TABLE html_path (
                id SERIAL NOT NULL, 
                acc_id INTEGER NOT NULL, 
                name VARCHAR NOT NULL,
                dir_path VARCHAR,
                PRIMARY KEY (id)
        );

CREATE INDEX ON html_path (acc_id);

CREATE TABLE template (
                id SERIAL NOT NULL, 
                acc_id INTEGER NOT NULL, 
                name VARCHAR(50), 
                down_rate INTEGER, 
                up_rate INTEGER, 
                down_limit INTEGER, 
                session_timeout INTEGER, 
                start_date TIMESTAMP WITHOUT TIME ZONE NOT NULL, 
                stop_date TIMESTAMP WITHOUT TIME ZONE NOT NULL, 
                PRIMARY KEY (id)
        );
CREATE INDEX ON template(acc_id);
CREATE INDEX ON template(start_date);
CREATE INDEX ON template(stop_date);

CREATE TABLE hotspot (
                id SERIAL NOT NULL, 
                acc_id INTEGER NOT NULL, 
                ip VARCHAR(50) NOT NULL, 
                name VARCHAR(20), 
                password VARCHAR NOT NULL, 
                address VARCHAR, 
                on_flag INTEGER, 
                usercache_name VARCHAR(30), 
                sms_api_url VARCHAR(150) NOT NULL, 
                sms_text VARCHAR NOT NULL, 
                template_id INTEGER, 
                html_path_id INTEGER, 
                start_date TIMESTAMP WITHOUT TIME ZONE NOT NULL, 
                stop_date TIMESTAMP WITHOUT TIME ZONE NOT NULL, 
                UNIQUE(ip),
                PRIMARY KEY (id), 
                FOREIGN KEY(template_id) REFERENCES template (id), 
                FOREIGN KEY(html_path_id) REFERENCES html_path (id)
        );
CREATE INDEX ON hotspot(acc_id);
CREATE INDEX ON hotspot(ip);
CREATE INDEX ON hotspot(start_date);
CREATE INDEX ON hotspot(stop_date);

CREATE TABLE radacct (
    RadAcctId bigserial PRIMARY KEY,
    AcctSessionId TEXT NOT NULL,
    AcctUniqueId TEXT NOT NULL UNIQUE,
    UserName TEXT,
    GroupName TEXT,
    Realm TEXT,
    NASIPAddress inet NOT NULL,
    NASPortId TEXT,
    NASPortType TEXT,
    AcctStartTime TIMESTAMP with time zone,
    AcctUpdateTime TIMESTAMP with time zone,
    AcctStopTime TIMESTAMP with time zone,
    AcctInterval BIGINT,
    AcctSessionTime BIGINT,
    AcctAuthentic TEXT,
    ConnectInfo_start TEXT,
    ConnectInfo_stop TEXT,
    AcctInputOctets BIGINT,
    AcctOutputOctets BIGINT,
    CalledStationId TEXT,
    CallingStationId TEXT,
    AcctTerminateCause TEXT,
    ServiceType TEXT,
    FramedProtocol TEXT,
    FramedIPAddress INET
);

CREATE UNIQUE INDEX radacct_whoson on radacct (AcctStartTime, nasipaddress);
CREATE INDEX radacct_active_session_idx ON radacct (AcctUniqueId) WHERE AcctStopTime IS NULL;
CREATE INDEX radacct_bulk_close ON radacct (NASIPAddress, AcctStartTime) WHERE AcctStopTime IS NULL;
CREATE INDEX radacct_start_user_idx ON radacct (AcctStartTime, UserName);



/*
 * INSERT DEFAULT VALUES
 */

-- user roles

INSERT INTO user_role VALUES (1, 'read-only', 'read-only', true, false, false, true, false, false, false, true, false, false, false, false);
INSERT INTO user_role VALUES (2, 'full-access', 'full-access', true, true, true, true, true, true, true, true, true, true, true, true);
INSERT INTO user_role VALUES (3, 'limit-1', 'deny htm_path create/delete, deny hotspot create/delete/change_ip', true, false, false, true, true, true, true, true, false, false, true, false);

-- account
INSERT INTO account VALUES (1, 'root_account', '2016-05-16 08:09:31.079113', '9999-12-31 00:00:00');

-- users
-- password 123456 (hashed by bcrypt)
INSERT INTO acc_user VALUES (1, 'root', '$2y$10$JOWZ.O0.TRl2TuJ4DtX5M.9W8kX8IaWNEX1CcsIXRV2HdsTJ4gFsa', 1, 2, '2016-05-16 08:09:31.079113', '9999-12-31 00:00:00');

-- default templates

INSERT INTO template (acc_id, name, down_rate, up_rate, down_limit, session_timeout, start_date, stop_date)
VALUES (1, 'default', 0, 0, 0, 0, '2016-05-13T16:12:48.501146'::timestamp, '9999-12-31T00:00:00'::timestamp);

INSERT INTO html_path (acc_id, dir_path, name) VALUES (1, NULL, 'default');






/*
 * INSERT TEST VALUES
 */


INSERT INTO hotspot (id, acc_id, ip, name, password, address, on_flag, usercache_name, sms_api_url, sms_text, template_id, html_path_id, start_date, stop_date) VALUES (1, 1, '10.162.31.13', 'NOC-WIFI', 'hfpldfnhb', 'unknown', 1, 'public', 'http://mltest:uq3owt5@socket.colibritelecom.ru:2040/SMSPortal?DST_PH=%(phone)s&TEXT=%(text)s', 'Code: %(code)s.  CREDO-TELECOM', 1, 1, '2016-05-16 08:09:31.079113', '9999-12-31 00:00:00');
-- INSERT INTO hotspot (id, acc_id, ip, name, password, address, on_flag, usercache_name, sms_api_url, sms_text, template_id, html_path_id, start_date, stop_date) VALUES (528983628, 1322688202, '194.67.162.246', 'BGruzin', 'Uks74KeKq', 'unknown', 1, 'public', 'http://fgburfi:Uks74KeKq@socket.colibritelecom.ru:2040/SMSPortal?DST_PH=%(phone)s&TEXT=%(text)s', 'Code: %(code)s.

-- CREDO-TELECOM', 1, 4, '2016-10-05 12:18:22.448023', '9999-12-31 00:00:00');
-- INSERT INTO hotspot (id, acc_id, ip, name, password, address, on_flag, usercache_name, sms_api_url, sms_text, template_id, html_path_id, start_date, stop_date) VALUES (509701736, 1256005998, '194.67.163.110', 'maksimov home', 'hfpldfnhb', 'unknown', 1, 'public', 'http://test:ttt1ttt@socket.colibritelecom.ru:2040/SMSPortal?DST_PH=%(phone)s&TEXT=%(text)s', 'Code: %(code)s.

-- CREDO-TELECOM', 1, 1, '2016-05-16 08:09:31.079113', '2017-05-10 17:44:20.180518');
-- INSERT INTO hotspot (id, acc_id, ip, name, password, address, on_flag, usercache_name, sms_api_url, sms_text, template_id, html_path_id, start_date, stop_date) VALUES (557835880, 1329152966, '194.67.168.62', 'MGHSWF', 'rOk29ueEiz', 'unknown', 1, 'public', 'http://mghswf:rOk29ueEiz@socket.colibritelecom.ru:2040/SMSPortal?DST_PH=%(phone)s&TEXT=%(text)s', 'Code: %(code)s.

-- CREDO-TELECOM', 1, 1, '2017-08-25 12:40:23.635995', '9999-12-31 00:00:00');
-- INSERT INTO hotspot (id, acc_id, ip, name, password, address, on_flag, usercache_name, sms_api_url, sms_text, template_id, html_path_id, start_date, stop_date) VALUES (556963185, 30035201, '10.162.112.50', 'RUM', 'fU4iIo2OOu', 'unknown', 1, 'public', 'http://rum_hs:fU4iIo2OOu@socket.colibritelecom.ru:2040/SMSPortal?DST_PH=%(phone)s&TEXT=%(text)s', 'Code: %(code)s.

-- CREDO-TELECOM', 1, 2, '2017-08-10 05:01:11.907163', '9999-12-31 00:00:00');
-- INSERT INTO hotspot (id, acc_id, ip, name, password, address, on_flag, usercache_name, sms_api_url, sms_text, template_id, html_path_id, start_date, stop_date) VALUES (528983777, 1322688202, '194.67.163.110', 'BGruzin-reserv', 'Uks74KeKq', 'unknown', 1, 'public', 'http://fgburfi:Uks74KeKq@socket.colibritelecom.ru:2040/SMSPortal?DST_PH=%(phone)s&TEXT=%(text)s', 'Code: %(code)s.

-- CREDO-TELECOM', 1, 4, '2016-10-05 12:18:22.448023', '9999-12-31 00:00:00');
-- INSERT INTO hotspot (id, acc_id, ip, name, password, address, on_flag, usercache_name, sms_api_url, sms_text, template_id, html_path_id, start_date, stop_date) VALUES (540027594, 1324993267, '194.67.156.42', 'BUFET', 'L34oqIo2', 'unknown', 1, 'public', 'http://bufeths:L34oqIo2@socket.colibritelecom.ru:2040/SMSPortal?DST_PH=%(phone)s&TEXT=%(text)s', 'Code: %(code)s.

-- CREDO-TELECOM', 1, 1, '2017-02-06 18:54:54.455334', '9999-12-31 00:00:00');
-- INSERT INTO hotspot (id, acc_id, ip, name, password, address, on_flag, usercache_name, sms_api_url, sms_text, template_id, html_path_id, start_date, stop_date) VALUES (513344592, 1256005998, '194.67.144.34', 'maksimov work', 'hfpldfnhb', 'unknown', 1, 'public', 'http://test:ttt1ttt@socket.colibritelecom.ru:2040/SMSPortal?DST_PH=%(phone)s&TEXT=%(text)s', 'Code: %(code)s.

-- CREDO-TELECOM', 1, 1, '2016-05-16 08:09:31.079113', '9999-12-31 00:00:00');



-- INSERT INTO html_path (id, acc_id, name, dir_path) VALUES (1, 1, 'default', NULL);
-- INSERT INTO html_path (id, acc_id, name, dir_path) VALUES (4, 1322688202, 'rfimnr', NULL);
-- INSERT INTO html_path (id, acc_id, name, dir_path) VALUES (5, 1256005998, 'rfimnr', NULL);
-- INSERT INTO html_path (id, acc_id, name, dir_path) VALUES (2, 30035201, 'credo', NULL);
-- INSERT INTO html_path (id, acc_id, name, dir_path) VALUES (6, 1329152966, 'mghswf', NULL);


-- INSERT INTO template (id, acc_id, name, down_rate, up_rate, down_limit, session_timeout, start_date, stop_date) VALUES (2, 1256005998, 'lala', 512, 512, 10240, 900, '2016-05-16 08:08:35.076638', '2016-05-16 08:09:13.347307');
-- INSERT INTO template (id, acc_id, name, down_rate, up_rate, down_limit, session_timeout, start_date, stop_date) VALUES (3, 1256005998, 'optima', 5120, 1024, 102400, 7200, '2016-05-16 08:10:08.225925', '2016-05-16 10:46:25.834524');
-- INSERT INTO template (id, acc_id, name, down_rate, up_rate, down_limit, session_timeout, start_date, stop_date) VALUES (5, 1256005998, 'минприроды', 512, 512, 10240, 900, '2016-09-28 14:01:23.21834', '2016-09-28 15:47:36.933612');
-- INSERT INTO template (id, acc_id, name, down_rate, up_rate, down_limit, session_timeout, start_date, stop_date) VALUES (4, 1256005998, 'оптима', 2048, 2048, 1048576, 7200, '2016-05-17 11:42:29.331298', '2016-10-05 11:48:44.67872');
-- INSERT INTO template (id, acc_id, name, down_rate, up_rate, down_limit, session_timeout, start_date, stop_date) VALUES (1, 1, 'безлимитный', 0, 0, 0, 0, '2016-05-13 16:12:48.501146', '9999-12-31 00:00:00');
-- INSERT INTO template (id, acc_id, name, down_rate, up_rate, down_limit, session_timeout, start_date, stop_date) VALUES (6, 1322688202, 'оптима', 2048, 2048, 204800, 900, '2016-10-20 14:26:50.493931', '2016-12-30 16:35:05.895693');
-- INSERT INTO template (id, acc_id, name, down_rate, up_rate, down_limit, session_timeout, start_date, stop_date) VALUES (7, 1324993267, 'лала', 512, 512, 10240, 900, '2017-02-06 19:00:26.565002', '2017-02-13 09:41:56.613342');
