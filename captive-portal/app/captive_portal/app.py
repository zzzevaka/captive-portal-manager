#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import logging
from os.path import dirname, join
from datetime import datetime

from tornado.web import Application, StaticFileHandler
from tornado.template import Loader

from sqlalchemy.orm import sessionmaker

from .handlers import *
from .models.base import Base
from .models.hotspot import SMSHotspot
from .billing import Billing
from tornado.locale import load_translations


class CaptivePortalApp(Application):

    def __init__(self, db_engine, redis_conn, **kwargs):
        # routes
        routes = [
                (r"/", MainHandler),
                (r"/login", LoginHandler),
                (r"/getcode", GetCodeHandler),
                (r"/check_code", CheckCodeHandler),
                (r"/confirm", ConfirmHandler),
                (r"/describe", DescribeHandler),
                (r"/captcha", CaptchaHandler),
                (r"/example", ExampleHandler),
                (r"/error", ErrorHandler),
                (r"/static/(.*)", StaticFileHandler,
                    {"path": join(dirname(__file__), 'static')}),
        ]
        # load translations from lang
        load_translations(join(dirname(__file__), 'lang'))
        # settings
        kwargs['login_url'] = '/login'
        kwargs['template_path'] = join(dirname(__file__), 'templates')
        # parent contructor
        super().__init__(routes, **kwargs)
        # database connection
        self.sessionmaker = sessionmaker(db_engine)
        self.cache = redis_conn
        self.hotspots = {}
        self.load_hotspots()

    def load_hotspots(self):
        '''load models SMSHotpost to self.hotspots'''
        logging.debug("hotspots loading")
        session = self.sessionmaker()
        tmp_list = session.query(SMSHotspot).filter(
                                        SMSHotspot.is_current()).all()
        session.close()
        self.hotspots = {h.ip: h for h in tmp_list}
        for h in tmp_list:
            logging.debug('hotspot %i (%s) has been loaded' % (h.id, h.ip))
        return 1
