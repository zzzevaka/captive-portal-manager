#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# captive-portal-mamger
#
# test model Hotspot

import logging
import unittest
from test_base import BaseTestCase

from captive_portal.models.hotspot import SMSHotspot

class ModelHotspotTestCase(BaseTestCase):

    #
    # CONSTRUCTOR
    #

    def test10_init_success(self):
        h = SMSHotspot(
            ip='127.0.0.1',
            password='12345',
            sms_api_url='localhost'
        )
        self.db_session.add(h)
        self.db_session.commit()

    def test11_init_ip_incorrect(self):
        with self.assertRaises(ValueError):
            h = SMSHotspot(
                ip='234234',
                password='12345',
                sms_api_url='localhost'
            )

    def test11_init_template_incorrect(self):
        with self.assertRaises(ValueError):
            h = SMSHotspot(
                ip='127.0.0.1',
                template_id='lala',
                password='12345',
                sms_api_url='localhost'
            )

    #
    # GET/SET
    #

    def test20_get_template(self):
        h = self.db_session.query(SMSHotspot).\
                filter_by(ip='127.0.0.1').first()
        h.template

    def test21_set_name_success(self):
        h = self.db_session.query(SMSHotspot).\
                filter_by(ip='127.0.0.1').first()
        h.name = 'test_hotspot'
        self.db_session.commit()
        self.assertEqual(h.name, 'test_hotspot')

    def test22_set_name_too_long(self):
        with self.assertRaises(ValueError):
            h = self.db_session.query(SMSHotspot).\
                    filter_by(ip='127.0.0.1').first()
            h.name = 'name'*20


if __name__ == '__main__':
    unittest.main()