#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# captive-portal-mamger
#
# base testing class:
# - connects to db
# - connects to redis
#
import os
import sys
import logging
import unittest

# sql
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine

# redis
from redis import StrictRedis

PREFIX = os.path.dirname(
            os.path.dirname(
                os.getcwd()
            )
         )
sys.path.append(PREFIX)

from etc.test import config
from captive_portal.models.base import Base


class BaseTestCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.db_session = cls.db_connect()
        cls.redis = cls.redis_connect()

    def db_connect():
        db_engine = create_engine('postgresql://%s:%s@%s/%s' % (
                                       config['DB']['user'],
                                       config['DB']['password'],
                                       config['DB']['host'],
                                       config['DB']['dbname']))
        Base.metadata.create_all(db_engine)
        return sessionmaker(db_engine)()

    def redis_connect():
        return StrictRedis(
                        host=config['REDIS']['host'],
                        port=config['REDIS']['port'],
                        decode_responses=True)

# initalize logging
logging.basicConfig(level=config['LOGGING']['level'],
                    format=config['LOGGING']['format'])

if __name__ == '__main__':
    unittest.main()
