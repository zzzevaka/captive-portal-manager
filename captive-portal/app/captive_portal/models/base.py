#!/usr/bin/env python
# -*- coding: utf-8 -*-

from datetime import datetime, MINYEAR, MAXYEAR

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, DateTime, and_
from sqlalchemy.ext.hybrid import hybrid_method
from sqlalchemy.ext.declarative import declared_attr

Base = declarative_base()


class AbstactStartStopModel(Base):

    __abstract__ = True

    @declared_attr
    def start_date(cls):
        return Column(DateTime, nullable=False)

    @declared_attr
    def stop_date(cls):
        return Column(DateTime, nullable=False)

    def __init__(self, start_date=datetime.now(),
                 stop_date=datetime(MAXYEAR, 12, 31)):
        self.start_date = start_date
        self.stop_date = stop_date

    def __setattr__(self, attr, value):
        if attr in ('start_date', 'stop_date'):
            if type(value).__name__ != 'datetime':
                raise TypeError('a datetime is required (got type %s)' %
                                type(value))
        super(AbstactStartStopModel, self).__setattr__(attr, value)

    def stop(self):
        self.stop_date = datetime.now()

    def revive(self):
        self.start_date = datetime.now()
        self.stop_date = datetime(MINYEAR, 12, 31)

    @hybrid_method
    def is_current(self):
        '''returns 1 if now is between start_date and stop_date'''
        if self.start_date < datetime.now() < self.stop_date:
            return 1

    @is_current.expression
    def is_current(cls):
        return and_(cls.start_date < datetime.now(),
                    cls.stop_date > datetime.now())
