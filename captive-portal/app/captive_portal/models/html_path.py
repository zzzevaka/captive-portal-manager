#!/usr/bin/env python
# -*- coding: utf-8 -*-

from sqlalchemy import Column, Integer, String, DateTime
from .base import Base

DEFAULT_ID = 1
DEFAULT_HTML_PATH_ID = 1


class HTML_Path(Base):

    DEFAULT_ID = 1
    DEFAULT_ACC_ID = 1
    DEFAULT_NAME = 'default'

    __tablename__ = 'html_path'

    id = Column(Integer, primary_key=True)
    acc_id = Column(Integer, index=True, nullable=False)
    name = Column(String, nullable=False)
    dir_path = Column(String)

    def get_path(self):
        return self.dir_path or self.name

    @classmethod
    def get_default(cls):
        return cls(id=cls.DEFAULT_ID, acc_id=cls.DEFAULT_ACC_ID,
                   name=cls.DEFAULT_NAME)
