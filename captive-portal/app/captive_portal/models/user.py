#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging
import bcrypt

from .base import Base, AbstactStartStopModel
from sqlalchemy import Column, Integer, String, Boolean, ForeignKey
from sqlalchemy.orm import relationship

class Role(Base):
    '''
        Model for defining users roles with different permissions
    '''

    DEFAULT_ROLE_ID = 0

    __tablename__ = 'user_role'

    id = Column(Integer, primary_key=True)
    name = Column(String(50))
    describe = Column(String(300))

    # html_path
    show_html_path = Column(Boolean, default=True)
    create_html_path = Column(Boolean, default=False)
    delete_html_path = Column(Boolean, default=False)

    # template
    show_template = Column(Boolean, default=True)
    create_template = Column(Boolean, default=False)
    delete_template = Column(Boolean, default=False)
    edit_template = Column(Boolean, default=False)

    # hotspot
    show_hotspot = Column(Boolean, default=True)
    create_hotspot = Column(Boolean, default=False)
    delete_hotspot = Column(Boolean, default=False)
    edit_hotspot = Column(Boolean, default=False)
    edit_hotspot_ip = Column(Boolean, default=False)

class User(AbstactStartStopModel):

    __tablename__ = 'acc_user'

    id = Column(Integer, primary_key=True)
    acc_id = Column(Integer, ForeignKey('account.id'))
    role_id = Column(Integer, ForeignKey('user_role.id'), default=1)
    username = Column(String(50))
    password = Column(String(150))

    account = relationship('Account')
    role = relationship('Role')

    @staticmethod
    def get_hash(string, salt=None):
        if isinstance(string, str):
            string = string.encode()
        if isinstance(salt, str):
            salt = salt.encode()
        return bcrypt.hashpw(string, salt or bcrypt.gensalt())

    def check_password(self, password):
        '''is hash of password from argument are equal to self.password'''
        logging.debug(self.get_hash(password, self.password))
        logging.debug(self.password)
        return self.get_hash(password, self.password) == self.password.encode()


class Account(AbstactStartStopModel):

    __tablename__ = 'account'

    id = Column(Integer, primary_key=True)
    name = Column(String(50))

    # templates = relationship(
    #                 'Template',
    #                 primaryjoin='User.acc_id == Template.acc_id'
    #             )
    
    # hotspots = relationship(
    #                 'SMSHotspot',
    #                 primaryjoin='User.acc_id == SMSHotspot.acc_id'
    #            )