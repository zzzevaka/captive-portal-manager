#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'''
    Rate Limitation feature

    usage example:

    from redis import StrictRedis
    from ratelimit import RateLimit, RateLimitReached

    @RateLimit.decorator
    def f():
        print('hello')

    redis = StrictRedis('localhost', 6379)

    RateLimit('test_s[1-9]', 10, 1, redis)
    RateLimit('test_10s[1-9]', 15, 10, redis)

    try:
        for x in range(1,12):
            print('call number: %i' % x)
            f(ratelimit=['test_s', 'test_10s'])
    except RateLimitReached as marker:
        print('rate limit reached by marker %s' % marker)
'''

import re
import time
import logging
from functools import wraps

REDIS_KEY = 'ratelimit:%s:%s'


class RateLimitReached (Exception):
    pass


class RateLimit(object):

    __limits = []

    def __init__(self, pattern, limit, seconds, redis):
        self.regexp = re.compile('^%s$' % self.normalize_key(pattern))
        self.limit = limit
        self.seconds = seconds
        self.redis = redis
        self.__limits.append(self)

    def incr(self, marker):
        marker = self.normalize_key(marker)
        current_key = REDIS_KEY % (marker, int(time.time()))
        for key in self.redis.keys(REDIS_KEY % (marker, '*')):
            if type(key) == bytes:
                key = key.decode()
            if self.is_current_key(key):
                current_key = key
        current_count = self.redis.incr(current_key)
        if current_count > self.limit:
            raise RateLimitReached(marker)
        elif current_count == 1:
            self.redis.expire(current_key, self.seconds)

    def is_current_key(self, key):
        logging.debug('key %s' % key)
        start_epoch = int(re.split(':', key)[2])
        logging.debug(
            ('key %s' % key) + '\n' +
            ('start epoch: %i' % start_epoch) + '\n' +
            ('left seconds: %i' % (time.time() - start_epoch + self.seconds))
        )
        return start_epoch + self.seconds > time.time()

    def match(self, marker):
        return self.regexp.match(marker)

    @staticmethod
    def normalize_key(key):
        return re.sub(':', '_', key)

    @classmethod
    def decorator(cls, f):
        @wraps(f)
        def wrapper(*args, **kwargs):
            arg = kwargs.pop('ratelimit', [])
            markers = arg if type(arg) in (list, tuple, set) else [arg]
            for marker in markers:
                logging.debug('ratelimit marker: %s' % marker)
                for limit in cls.__limits:
                    if limit.match(marker):
                        logging.debug('increase marker %s' % marker)
                        limit.incr(marker)
            return f(*args, **kwargs)
        return wrapper
