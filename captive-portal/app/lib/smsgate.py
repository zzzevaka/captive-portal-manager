#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import re
import time
import logging
from urllib.parse import quote

from tornado.httpclient import AsyncHTTPClient, HTTPRequest
from tornado.gen import coroutine

from lib.ratelimit import RateLimit


class SMSGateError(Exception):
    pass


class SMSGate(object):

    def __init__(
                self, api_url, max_text_length=70,
                phone_regexp="^\+\d{5,15}"):
        '''
        valid format of URL:
            http://user:pwd@socket.colibritelecom.ru:2040/SMSPortal?DST_PH=%(phone)s&TEXT=%(text)s
        '''
        self.url = api_url
        self.max_text_length = max_text_length
        self.phone_regexp = phone_regexp

    @RateLimit.decorator
    @coroutine
    def send(self, phone, text):
        logging.debug('send sms to %s: %s' % (phone, text))
        # check the number
        if not self.is_valid_number(phone):
            raise ValueError('%s is not a valid phone number')
        # check the text
        if not self.is_valid_text(text):
            raise ValueError('invalid text: "%s"' % text)
        try:
            logging.debug('SMS_URL: %s' % self.url)
            norm_url = quote(self.url % {'phone': phone, 'text': text},
                             safe=':/@=?&')
            request = HTTPRequest(norm_url)
            http_client = AsyncHTTPClient()
            response = yield http_client.fetch(request)
            if response.code == 200:
                return 1
            else:
                raise SMSGateError('SMSPortal has returned not 200\nURL: \
                    %s\nResponse code: %i' % (request.url, response.code))
        except:
            logging.error('An error has occured while SMSPortal \
                        using:\nURL: %s\n' % request.url, exc_info=True)
            raise

    def is_valid_text(self, text):
        # logging.debug("text length %i (%s)" % (len(text), text))
        return 1 if len(text) <= self.max_text_length else 0

    def is_valid_number(self, phone):
        return 1 if re.match(self.phone_regexp, phone) else 0
