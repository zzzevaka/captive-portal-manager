#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# config utils

import logging
from contextlib import contextmanager

@contextmanager
def exc_cfg_key_error(cfg_section=None):
    '''
        contex manager with excepting KeyError.

        If KeyError raised log fatal message and exit

        Parameters
        ----------
        cfg_section: str
            SECTION printed in message
    '''
    try:
        yield
    except KeyError as key:
        section_info = ''
        if cfg_section:
            section_info = ' at section {s}'.format(s=cfg_section)
        logging.fatal('Config error{s}: key {k} not fount'.format(
            s=section_info,
            k=key
        ))
        exit(1)