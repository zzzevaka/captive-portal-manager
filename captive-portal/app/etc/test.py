#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# captive-portal-mamger
#
# settings for tests
import logging


config = {
    'HTTP': {
        'port': 80,
        'host': '*'
    },
    'DB': {
        'host': 'db',
        'dbname': 'captive-portal',
        'user': 'captive-portal',
        'password': 'hfpldfnhb'
    },
    'REDIS': {
        'host': 'redis',
        'port': '6379',
    },
    'RATE_LIMIT': {
        'sms_hour': (100,3600),
        'sms_day': (1000,86400)
    },
    'LOGGING': {
        'format': (u'%(filename)s[LINE:%(lineno)d]# ' +
                   u'%(levelname)s [%(asctime)s]  %(message)s'),
        'level': logging.DEBUG
    },
}