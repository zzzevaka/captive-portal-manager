#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# captive-portal-mamger
#
# Server to handle reqests from hotspots

import os
import sys
import signal
import logging
from argparse import ArgumentParser
from configparser import SafeConfigParser

from tornado.httpserver import HTTPServer
from tornado.ioloop import IOLoop, PeriodicCallback

from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine

from redis import StrictRedis

from captive_portal.app import CaptivePortalApp
from captive_portal.billing import Billing
from captive_portal.models.base import Base
from lib.ratelimit import RateLimit
from lib.config_utils import exc_cfg_key_error


PREFIX_DIR = os.path.dirname(os.path.realpath(__file__))
DEFAULT_CFG_FILE = os.path.join(PREFIX_DIR, 'etc/captive_portal.ini')
DB_ENGINE_DIALECT = 'postgresql://{user}:{password}@{host}/{dbname}' 
LOG_FMT_DEFAULT = '%(levelname)s [%(asctime)s]  %(message)s'
LOG_FMT_VERBOSE = '%(filename)s[LINE:%(lineno)d]# ' + \
                  '%(levelname)s [%(asctime)s]  %(message)s'


def stop_ioloop(signum, frame):
    logging.info('stop HTTP server')
    IOLoop.current().stop()


if __name__ == '__main__':
    try:
        # signal handlers
        signal.signal(signal.SIGINT, stop_ioloop)
        signal.signal(signal.SIGTERM, stop_ioloop)

        # parse command line arguments
        parser = ArgumentParser(description="camipot server")
        parser.add_argument("-c", "--cfg_file", type=str,
                            action="store", help="path to config file")
        parser.add_argument("-d", "--debug",
                            action="store_true", help="show debug info")
        parser.add_argument("-v", "--verbose",
                            action="store_true", help="show details")
        args = parser.parse_args()

        # logging initialization
        logging_formatter = LOG_FMT_VERBOSE if args.verbose else LOG_FMT_DEFAULT
        logging.basicConfig(
            level=logging.DEBUG if args.debug else logging.INFO,
            format=logging_formatter
        )

        # load config file
        cfg_file = args.cfg_file if args.cfg_file else DEFAULT_CFG_FILE
        cfg = SafeConfigParser()
        if not cfg.read(cfg_file):
            logging.error("couldn't load config from %s" % cfg_file)
            sys.exit(1)

        # add logging file handler if a logging directory was specified
        # and access to wiriting is granted
        if 'LOGGING' in cfg and 'logdir'in cfg['LOGGING']:
            logdir = cfg['LOGGING']['logdir']
            if os.access(logdir, os.W_OK):
                log_file = os.path.join(logdir, 'captive_portal.log')
                # one day = one log file
                fh = logging.handlers.TimedRotatingFileHandler(
                                                        log_file,
                                                        when='midnight')
                fh.setFormatter(logging.Formatter(logging_formatter))
                root_logger = logging.getLogger()
                root_logger.addHandler(fh)
            else:
                logging.error("can't write logs to %s" % logdir)

        # database
        db_engine = None
        redis_conn = None
        with exc_cfg_key_error():
            with exc_cfg_key_error('DATABASE'):
                db_engine = create_engine(DB_ENGINE_DIALECT.format(**cfg['DATABASE']))
            with exc_cfg_key_error('REDIS'):
                redis_conn = StrictRedis(**cfg['REDIS'], decode_responses=True)

        # ioloop
        ioloop = IOLoop().current()

        # rate limititation
        if 'RATE_LIMIT' in cfg:
            for pattern, val in cfg['RATE LIMIT'].items():
                try:
                    cnt, sec = [int(x) for x in val.split(',')]
                    RateLimit(pattern, cnt, sec, redis_conn)
                    logging.debug('rate limit was set: %s' % pattern)
                except:
                    logging.warning('an incorrect rate limit %s' % pattern,
                                    exc_info=args.debug)

        # the app
        settings = {'debug': args.debug}
        app = CaptivePortalApp(db_engine, redis_conn, **settings)

        # p_eriodic callback for reloading hotspots
        with exc_cfg_key_error():
            cfg['PERIODIC']
        with exc_cfg_key_error('PERIODIC'):
            hotspots_reload_per = PeriodicCallback(
                            app.load_hotspots,
                            1000*int(cfg['PERIODIC']['hotspots_reload']),
                            ioloop)
            hotspots_reload_per.start()

        # HTTP server
        httpserver = HTTPServer(app, xheaders=True)
        httpserver.listen(cfg['HTTP']['port'], cfg['HTTP']['host'])
        logging.info('HTTP server started on %s:%s' % (
                                            cfg['HTTP']['host'],
                                            cfg['HTTP']['port']))
        ioloop.start()
    except Exception as err:
        logging.fatal('An unexpected error has occured', exc_info=True)
        exit(1)
