#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import logging
from os.path import dirname, join

from tornado.locale import load_translations
from tornado.web import Application, StaticFileHandler
from sqlalchemy.orm import sessionmaker

from .handlers import *


class WebCliApp(Application):

    def __init__(self, db_engine, redis_conn, **kwargs):
        template_path = join(dirname(__file__), 'templates')
        routes = [
            (r"/login", LoginHandler),
            (r"/logout", LogoutHandler),
            (r"/", StatHandler),
            (r"/manage", ManageHandler),
            (r"/stat", StatHandler),
            (r"/new_template", NewTemplateHandler),
            (r"/edit_template/([0-9]+)", EditTemplateHandler),
            (r"/del_template/([0-9]+)", DelTemplateHandler),
            (r"/new_hotspot", NewHotspotHandler),
            (r"/edit_hotspot/([0-9]+)", EditHotspotHandler),
            (r"/del_hotspot/([0-9]+)", DelHotspotHandler),
            (r"/get_session_table", GetUserSessionInfo),
            (r"/get_session_id", GetSessionHandler),
            (r"/(.*)", StaticFileHandler, {"path": template_path}),
        ]
        # load translations from lang
        load_translations(join(dirname(__file__), 'lang'))
        kwargs['login_url'] = '/login'
        kwargs['template_path'] = template_path + '/standalone'
        super().__init__(routes, **kwargs)
        self.sessionmaker = sessionmaker(db_engine)
        self.cache = redis_conn
