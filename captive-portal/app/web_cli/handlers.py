#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from time import time
import logging
import functools
from ipaddress import ip_address, ip_network

import tornado.web
import psycopg2
from sqlalchemy import and_, or_
import sqlalchemy.exc
from sqlalchemy.orm import joinedload

from lib.session import SessionHandler
from captive_portal.models.user import User, Account, Role
from captive_portal.models.template import Template
from captive_portal.models.hotspot import SMSHotspot
from captive_portal.models.html_path import HTML_Path

from lib.dataTable import TornadoDataTable


ASR_NETWORK = ip_network('158.250.255.0/24')
CLI_SESSION_TIMEOUT = 3600*12
CLI_SESSION_LONG_TIMEOUT = 3600*24*90
DEFAULT_ACC_ID = 1


def require_user_permissions(*permissions):
    '''
        Decorator.
        Checks User.role permissions before running decorated function

        Must be decorated by @tornado.web.authenticated. 

        Example:
        @require_user_permissions('edit_hotspot', 'show_hotspot')
    '''
    def decorator(f):
        @functools.wraps(f)
        def wrapper(handler, *args, **kwargs):

            for p in permissions:
                if not getattr(handler.current_user.role, p) is True:
                    raise tornado.web.HTTPError(403)
            return f(handler, *args, **kwargs)
        return wrapper
    return decorator

def error_template_by_exception(f):

    @functools.wraps(f)
    def wrapper(handler, *args, **kwargs):
        try:
            return f(handler, *args, **kwargs)
        except tornado.web.HTTPError:
            raise
        except Exception as err:
            # errmsg = 'Unexpected error'
            logging.error('An unexpected error has occured', exc_info=True)
            handler.db.rollback()
            handler.render(
                'error.html',
                user=handler.current_user,
                errmsg=err,
                session_id=handler.session.sid
            )

    return wrapper


class BaseCliHandler(SessionHandler):

    def initialize(self):
        self.db = self.application.sessionmaker()

    def on_finish(self):
        self.db.close()
        super(BaseCliHandler, self).on_finish()

    def get_current_user(self):
        if self.session['user_id']:
            self.user = self.db.query(User).\
                            join(Account).\
                            join(Role).\
                            options(joinedload(User.role)).\
                            filter(User.is_current()).\
                            filter(Account.is_current()).\
                            first()
            return self.user

    def get_hotspot(self, id):
        return self.db.query(SMSHotspot).filter(
            SMSHotspot.id == id,
            SMSHotspot.is_current(),
            SMSHotspot.acc_id == self.current_user.acc_id
        ).first()
    
    def get_templates(self):
        return self.db.query(Template).filter(
            or_(
                Template.acc_id == self.current_user.acc_id,
                Template.acc_id == DEFAULT_ACC_ID),
            Template.is_current()
        ).all()

    def get_html_paths(self):
        return self.db.query(HTML_Path).filter(
            or_(
                HTML_Path.acc_id == self.current_user.acc_id,
                HTML_Path.acc_id == DEFAULT_ACC_ID
            ),
        ).all()

class LoginHandler(BaseCliHandler):

    def get(self):
        self.render('login.html',
                    user=self.current_user,
                    session_id=self.session.sid)

    def post(self):
        username = self.get_argument('username')
        password = self.get_argument('password')
        user = self.db.query(User).\
                join(Account).\
                filter(User.username == username).\
                filter(User.is_current()).\
                filter(Account.is_current()).\
                first()
        if not user:
            raise tornado.web.HTTPError(403)
        if user.check_password(password):
            self.session['user_id'] = user.id
            self.redirect('/')
        else:
            self.redirect('/')


class LogoutHandler(BaseCliHandler):

    def get(self):
        self.clear_all_cookies()
        self.redirect('/')


class GetSessionHandler(SessionHandler):

    def get_current_user(self):
        '''
            just returns session_id

            request from not trusted hosts will be rejected
            look at ASR_NETWORK

            WARNING: SessioHandler hashes an session id by User-Agent.
            You have to fake user's User-Agent
        '''
        return ip_address(self.request.remote_ip) in ASR_NETWORK

    @tornado.web.authenticated
    def get(self):
        self.session['acc_id'] = self.get_argument('acc_id')
        self.session['username'] = 'maksimov'
        self.session.set_ttl(CLI_SESSION_TIMEOUT)
        self.finish(self.session.sid)


class StatHandler(BaseCliHandler):

    @tornado.web.authenticated
    def get(self):
        self.render('stat.html',
                    user=self.current_user,
                    session_id=self.session.sid)


class ManageHandler(BaseCliHandler):

    @tornado.web.authenticated
    @error_template_by_exception
    def get(self):
        now = time()
        templates = self.db.query(Template).filter(
                            or_(Template.acc_id == self.current_user.acc_id,
                                Template.acc_id == DEFAULT_ACC_ID),
                            Template.is_current()
                        ).order_by(Template.id).all()
        html_paths = self.db.query(HTML_Path).filter(
                            or_(HTML_Path.acc_id == self.current_user.acc_id,
                                HTML_Path.acc_id == DEFAULT_ACC_ID),
                        ).order_by(HTML_Path.id).all()
        hotspots = self.db.query(SMSHotspot).filter(
                            SMSHotspot.is_current(),
                            SMSHotspot.acc_id == self.current_user.acc_id
                        ).order_by(SMSHotspot.id).all()
        self.render('manage2.html',
                    user=self.current_user,
                    templates=templates,
                    html_paths=html_paths,
                    hotspots=hotspots,
                    session_id=self.session.sid)


class NewTemplateHandler(BaseCliHandler):

    @tornado.web.authenticated
    @error_template_by_exception
    @require_user_permissions('create_template')
    def get(self):
        self.render(
            'new_template.html',
            session_id=self.session.sid,
            user=self.current_user,
        )
    
    @tornado.web.authenticated
    @error_template_by_exception
    @require_user_permissions('create_template')
    def post(self):
        template = None
        args = {}
        for val in ('name', 'down_rate', 'up_rate',
                    'down_limit', 'session_timeout'):
            args[val] = self.get_argument(val)
            if not args[val]:
                raise Exception("Field '%s' must be filled" % val)
        template = Template(
                        name=args['name'],
                        acc_id=self.current_user.acc_id,
                        down_rate=args['down_rate'],
                        up_rate=args['up_rate'],
                        down_limit=args['down_limit'],
                        session_timeout=args['session_timeout'])
        self.db.add(template)
        self.db.commit()
        self.redirect('/manage')
        return


class DelTemplateHandler(BaseCliHandler):

    @tornado.web.authenticated
    @error_template_by_exception
    @require_user_permissions('delete_template')
    def get(self, id):
        template = self.db.query(Template).\
                        filter(Template.id == id).\
                        filter(Template.is_current()).\
                        filter(SMSHotspot.is_current()).\
                        filter(Template.acc_id == self.current_user.acc_id).\
                        first()
        if not template:
            raise tornado.web.HTTPError(404)
        if not template.is_used():
            template.stop()
            self.db.commit()
            self.redirect('/manage')
        else:
            raise Exception('This template is used by hotspots.')


class EditTemplateHandler(BaseCliHandler):

    @tornado.web.authenticated
    @error_template_by_exception
    @require_user_permissions('show_template', 'edit_template')
    def get(self, id):
        template = self.db.query(Template).filter(
            Template.id == id,
            Template.is_current(),
            Template.acc_id == self.current_user.acc_id
        ).first()
        self.render('edit_template.html',
                    user=self.current_user,
                    session_id=self.session.sid,
                    template=template)
        if not template:
            raise tornado.web.HTTPError(404)

    @tornado.web.authenticated
    @error_template_by_exception
    @require_user_permissions('show_template', 'edit_template')
    def post(self, id):
        template = self.db.query(Template).filter(
                            Template.id == id,
                            Template.is_current(),
                            Template.acc_id == self.current_user.acc_id
                        ).first()
        if not template:
            raise tornado.web.HTTPError(404)
        for attr in ('name', 'down_rate', 'up_rate', 'down_limit',
                        'session_timeout'):
            if attr in ('name') and not self.get_argument(attr):
                raise Exception('Field %s must be filled' % attr)
            setattr(template, attr, self.get_argument(attr))
        self.db.commit()
        self.redirect('/manage')


class EditHotspotHandler(BaseCliHandler):

    @tornado.web.authenticated
    @error_template_by_exception
    @require_user_permissions('show_hotspot', 'edit_hotspot')
    def get(self, id):
        hotspot = self.get_hotspot(id)
        if not hotspot:
            raise tornado.web.HTTPError(404)
        self.render('edit_hotspot.html',
                    user=self.current_user,
                    session_id=self.session.sid,
                    hotspot=hotspot,
                    templates=self.get_templates(),
                    html_paths=self.get_html_paths())

    @tornado.web.authenticated
    @error_template_by_exception
    @require_user_permissions('show_hotspot', 'edit_hotspot')
    def post(self, id):
        try:
            hotspot = self.get_hotspot(id)
            if not hotspot:
                raise tornado.web.HTTPError(404)
            for attr in ('name', 'ip', 'template_id', 'html_path_id'):
                if attr == 'ip' and not self.current_user.role.edit_hotspot_ip:
                    raise tornado.web.HTTPError(403)
                setattr(hotspot, attr, self.get_argument(attr))
            self.db.commit()
            self.redirect('/manage')
        except sqlalchemy.exc.IntegrityError as err:
            if 'duplicate key value violates unique constraint' in str(err):
                raise Exception('duplicate IP')
            raise

class NewHotspotHandler(BaseCliHandler):

    @tornado.web.authenticated
    @error_template_by_exception
    @require_user_permissions('create_hotspot')
    def get(self):
        self.render(
            'new_hotspot.html',
            session_id=self.session.sid,
            user=self.current_user,
            templates=self.get_templates(),
            html_paths=self.get_html_paths()
        )
    
    @tornado.web.authenticated
    @error_template_by_exception
    @require_user_permissions('create_hotspot')
    def post(self):
        try:
            args = {}
            for val in ('name', 'ip', 'template_id', 'html_path_id'):
                args[val] = self.get_argument(val)
                if not args[val]:
                    if val != 'name':
                        raise Exception("Field '%s' must be filled" % val)
            hotspot = SMSHotspot(
                            acc_id=self.current_user.acc_id,
                            password='dummy_password',
                            sms_api_url='dummy_sms_api_url',
                            sms_text='dummy_sms_text',
                            **args
                    )
            self.db.add(hotspot)
            self.db.commit()
            self.redirect('/manage')
        except sqlalchemy.exc.IntegrityError as err:
            if 'duplicate key value violates unique constraint' in str(err):
                raise Exception('duplicate IP')
            raise

class DelHotspotHandler(BaseCliHandler):
    
    @tornado.web.authenticated
    @error_template_by_exception
    @require_user_permissions('delete_hotspot')
    def get(self, id):
        hotspot = self.db.query(SMSHotspot).filter(
                            SMSHotspot.id == id,
                            SMSHotspot.is_current(),
                            SMSHotspot.acc_id == self.current_user.acc_id
                        ).first()
        if not hotspot:
            raise tornado.web.HTTPError(403)
        hotspot.stop()
        self.db.commit()
        self.redirect('/manage')

class GetUserSessionInfo(BaseCliHandler):

    @tornado.web.authenticated
    def post(self):
        request = '''
            (SELECT
                DISTINCT ON (radacctid) radacctid, username,
                hotspot.name hotspot_name, callingstationid,
                to_char(acctstarttime, 'YYYY-MM-DD HH24:MI:SS') start_session,
                to_char(acctstoptime, 'YYYY-MM-DD HH24:MI:SS') stop_session,
                acctterminatecause
            FROM radacct JOIN hotspot
                ON radacct.nasipaddress = inet(hotspot.ip)
            WHERE hotspot.acc_id = %i
            ORDER BY radacctid DESC, stop_date DESC) t
        ''' % int(self.current_user.acc_id)
        logging.debug('get session info of users')
        dataTable = TornadoDataTable(
            sql_conn=self.db.connection().connection,
            columns=[
                    'hotspot_name',
                    "substring(username from '(\d+)@')",
                    'callingstationid',
                    'start_session',
                    'stop_session'],
            from_block=request,
            arguments=self.request.arguments
        )
        json_data = dataTable.get_data()
        self.finish(json_data)
